`timescale 1s / 1ps

module TH
#(
	parameter Precision = 32,
	parameter MaxLength = 4,
	parameter MaxWidth = 4,
	parameter MaxHeight = 4,
	parameter BlockWidth = 8,
	parameter AXIL_AddressWidth = 12,
	parameter AXIL_DataWidth = 32
)(
	input wire ACLK,
	input wire RSTN,

	output wire Error
);

wire [Precision*BlockWidth-1:0]S_AXIS_TDATA;
wire [Precision*BlockWidth/8-1:0]S_AXIS_TKEEP;
wire S_AXIS_TVALID;
wire S_AXIS_TLAST;
wire S_AXIS_TREADY;

wire [Precision*BlockWidth-1:0]M_AXIS_TDATA;
wire [Precision*BlockWidth/8-1:0] M_AXIS_TKEEP;
wire M_AXIS_TVALID;
wire M_AXIS_TLAST;
wire M_AXIS_TREADY;

AXIS_Source AXIS_Master
(
	.aclk(ACLK),
	.aresetn(RSTN),
	.m_axis_tvalid(M_AXIS_TVALID),
	.m_axis_tready(M_AXIS_TREADY),
	.m_axis_tdata(M_AXIS_TDATA),
	.m_axis_tkeep(M_AXIS_TKEEP),
	.m_axis_tlast(M_AXIS_TLAST)
);

AXIS_Sink AXIS_Slave
(
	.aclk(ACLK),
	.aresetn(RSTN),
	.s_axis_tvalid(S_AXIS_TVALID),
	.s_axis_tready(S_AXIS_TREADY),
	.s_axis_tdata(S_AXIS_TDATA),
	.s_axis_tkeep(S_AXIS_TKEEP),
	.s_axis_tlast(S_AXIS_TLAST)
);

wire [AXIL_AddressWidth-1:0]M_AXIL_AWADDR;
wire M_AXIL_AWVALID;
wire M_AXIL_AWREADY;

wire [31:0]M_AXIL_WDATA;
wire M_AXIL_WVALID;
wire M_AXIL_WREADY;

wire [1:0]M_AXIL_BRESP;
wire M_AXIL_BVALID;
wire M_AXIL_BREADY;

wire [AXIL_AddressWidth-1:0]M_AXIL_ARADDR;
wire M_AXIL_ARVALID;
wire M_AXIL_ARREADY;

wire [31:0]M_AXIL_RDATA;
wire [1:0]M_AXIL_RRESP;
wire M_AXIL_RVALID;
wire M_AXIL_RREADY;
	
M_AXIL AXIL_VIP
(
	.aclk(ACLK),
	.aresetn(RSTN),

	.m_axi_awaddr(M_AXIL_AWADDR),
	.m_axi_awvalid(M_AXIL_AWVALID),
	.m_axi_awready(M_AXIL_AWREADY),

	.m_axi_wdata(M_AXIL_WDATA),
	.m_axi_wvalid(M_AXIL_WVALID),
	.m_axi_wready(M_AXIL_WREADY),

	.m_axi_bresp(M_AXIL_BRESP),
	.m_axi_bvalid(M_AXIL_BVALID),
	.m_axi_bready(M_AXIL_BREADY),

	.m_axi_araddr(M_AXIL_ARADDR),
	.m_axi_arvalid(M_AXIL_ARVALID),
	.m_axi_arready(M_AXIL_ARREADY),

	.m_axi_rdata(M_AXIL_RDATA),
	.m_axi_rresp(M_AXIL_RRESP),
	.m_axi_rvalid(M_AXIL_RVALID),
	.m_axi_rready(M_AXIL_RREADY)
);

Laplace
#(
	.Max_Length(MaxLength),
	.Max_Width(MaxWidth),
	.Max_Height(MaxHeight),
	.Block_Width(BlockWidth)
)DUT(
	.ACLK(ACLK),
	.RSTN(RSTN),
	.Error(Error),

	.S_AXIS_TDATA(M_AXIS_TDATA),
	.S_AXIS_TKEEP(M_AXIS_TKEEP),
	.S_AXIS_TVALID(M_AXIS_TVALID),
	.S_AXIS_TLAST(M_AXIS_TLAST),
	.S_AXIS_TREADY(M_AXIS_TREADY),

	.M_AXIS_TDATA(S_AXIS_TDATA),
	.M_AXIS_TKEEP(S_AXIS_TKEEP),
	.M_AXIS_TVALID(S_AXIS_TVALID),
	.M_AXIS_TLAST(S_AXIS_TLAST),
	.M_AXIS_TREADY(S_AXIS_TREADY),

	.S_AXIL_AWADDR(M_AXIL_AWADDR),
	.S_AXIL_AWVALID(M_AXIL_AWVALID),
	.S_AXIL_AWREADY(M_AXIL_AWREADY),

	.S_AXIL_WDATA(M_AXIL_WDATA),
	.S_AXIL_WVALID(M_AXIL_WVALID),
	.S_AXIL_WREADY(M_AXIL_WREADY),

	.S_AXIL_BRESP(M_AXIL_BRESP),
	.S_AXIL_BVALID(M_AXIL_BVALID),
	.S_AXIL_BREADY(M_AXIL_BREADY),

	.S_AXIL_ARADDR(M_AXIL_ARADDR),
	.S_AXIL_ARVALID(M_AXIL_ARVALID),
	.S_AXIL_ARREADY(M_AXIL_ARREADY),

	.S_AXIL_RDATA(M_AXIL_RDATA),
	.S_AXIL_RRESP(M_AXIL_RRESP),
	.S_AXIL_RVALID(M_AXIL_RVALID),
	.S_AXIL_RREADY(M_AXIL_RREADY)
);

endmodule
