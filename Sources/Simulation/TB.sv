`timescale 1s / 1ps

import axi_vip_pkg::*;
import axi4stream_vip_pkg::*;
import M_AXIL_pkg::*;
import AXIS_Source_pkg::*;
import AXIS_Sink_pkg::*;

module TB();

parameter Frequency = 250e6;
parameter Period = 1/Frequency/2;
parameter SoftResetPeriod = 10*Period;
parameter HardResetPeriod = 100*Period;

parameter BlockWidth = 8;
parameter Precision = 32;
parameter DataWidth = BlockWidth*Precision;

parameter AXIL_AddressWidth = 12;
parameter AXIL_DataWidth = 32;

parameter Iterations = 1;
parameter Random = 1;
parameter Constant = 0;
parameter Tolerance = 1e-2;
parameter Verbose = 0;
parameter VerboseVIP = 0;
parameter ExportData = 1;

parameter MaxLength = 16;
parameter MaxWidth = 16;
parameter MaxHeight = 16;
parameter Length = 8;
parameter Width = 8;
parameter Height = 8;

reg RSTN;
reg CLK;
wire Error;

shortreal NaN = $bitstoshortreal(32'h0x7FC00000);

TH #
(
	.Precision(Precision),
	.MaxLength(MaxLength),
	.MaxWidth(MaxWidth),
	.MaxHeight(MaxHeight),
	.BlockWidth(BlockWidth),
	.AXIL_AddressWidth(AXIL_AddressWidth),
	.AXIL_DataWidth(AXIL_DataWidth)
)DUT(
	.RSTN(RSTN),
	.ACLK(CLK),

	.Error(Error)
);

always
begin
	#Period
	CLK = ~CLK;
end

always @(posedge |Error)
begin
	$error("Hardware error.");
	$finish;
end

initial begin
	shortreal Matrix[Iterations][Height][Length][Width*BlockWidth];
	shortreal Expected[Iterations][Height][Length][Width*BlockWidth];
	shortreal Received[Iterations][Height][Length][Width*BlockWidth];
	integer A, B, C;

	AXIS_Source_mst_t SourceAgent;
	AXIS_Sink_slv_t SinkAgent;
	M_AXIL_mst_t MasterAgent;

	//Initialization of agents:
	SourceAgent = new("Master Agent", DUT.AXIS_Master.inst.IF);
	SinkAgent = new("Slave Agent", DUT.AXIS_Slave.inst.IF);
	MasterAgent = new("Master Agent", DUT.AXIL_VIP.inst.IF);

	SourceAgent.set_agent_tag("Source");
	SinkAgent.set_agent_tag("Sink");
	MasterAgent.set_agent_tag("Master");

	SourceAgent.set_verbosity(VerboseVIP ? 400 : 0);
	SinkAgent.set_verbosity(VerboseVIP ? 400 : 0);
	MasterAgent.set_verbosity(VerboseVIP ? 400 : 0);
	
	SourceAgent.vif_proxy.set_dummy_drive_type(XIL_AXI4STREAM_VIF_DRIVE_NONE);
	SinkAgent.vif_proxy.set_dummy_drive_type(XIL_AXI4STREAM_VIF_DRIVE_NONE);

	SourceAgent.start_master();
	SinkAgent.start_slave();
	MasterAgent.start_master();

	//Initialization and reset sequence:
	CLK = 0;
	RSTN = 0;

	#HardResetPeriod
	@(negedge CLK);
	RSTN = 1;

	#SoftResetPeriod
	Reset(MasterAgent);
	#SoftResetPeriod
	SetDimensions(MasterAgent, Length, Width, Height);
	GetMaxDimensions(MasterAgent, A, B, C);
	$display("HW Max Length: ", A, " Max Width: ", B, " Max Height: ", C);
	GetDimensions(MasterAgent, A, B, C);
	$display("Set Length: ", A, " Set Width: ", B, " Set Height: ", C);
	#SoftResetPeriod

	for (integer I = 0; I < Iterations; I++) begin
		if (Verbose) begin
			$display("Iteration %d:", I);
		end

		GenerateData(Random, Constant, Matrix[I]);
		CalculateResult(Matrix[I], Expected[I]);
		InitReceived(Received[I]);

		if (Verbose) begin
			$display("Matrix:");
			PrintData(Matrix[I]);
			$display("Expected:");
			PrintData(Expected[I]);
		end
		
		if (ExportData) begin
			string S;
			S.itoa(I);
			ExportDataCSV(Matrix[I], {"Matrix(", S, ").csv"});
			ExportDataCSV(Expected[I], {"Expected(", S, ").csv"});
		end
	end

	fork
		//Send data:
		begin
			for (integer I = 0; I < Iterations; I++) begin
				SendData(Matrix[I], SourceAgent);
			end
		end
		//Get data:
		begin
			for (integer I = 0; I < Iterations; I++) begin
				GetData(Received[I], SinkAgent);
			end
		end
		//Generare backpressure:
		RandomBackpressure(SinkAgent);
	join
	
	for (integer I = 0; I < Iterations; I++) begin
		if (Verbose) begin
			$display("Iteration %d:", I);

			$display("Received:");
			PrintData(Received[I]);
		end

		if (ExportData) begin
			string S;
			S.itoa(I);
			ExportDataCSV(Received[I], {"Received(", S, ").csv"});
		end

		//Compare data:
		if (CompareData(Received[I], Expected[I])) begin
			$info("Success");
		end
		#Period;
	end

	$stop;
end

task automatic RandomBackpressure;
		ref AXIS_Sink_slv_t SinkAgent;
	begin
		axi4stream_ready_gen Gen;
		Gen = SinkAgent.driver.create_ready("Random Backpressure");
		Gen.set_ready_policy(XIL_AXI4STREAM_READY_GEN_RANDOM);
		Gen.set_low_time_range(0, 1);
		Gen.set_high_time_range(1, 1);
		Gen.set_event_count_range(4, 4);
		SinkAgent.driver.send_tready(Gen);
	end
endtask;

function automatic void GenerateData;
		input reg Random;
		input reg Constant;
		ref shortreal Matrix[Height][Length][Width*BlockWidth];
	begin
		for (integer I = 0; I < Height; I++) begin
			for (integer J = 0; J < Length; J++) begin
				for (integer K = 0; K < Width*BlockWidth; K++) begin
					if (Random) begin
						Matrix[I][J][K] = $urandom_range(0, 100) / 100.0;
					end else if (Constant) begin
						Matrix[I][J][K] = 1.0;
					end else begin
						Matrix[I][J][K] = I*Height*Width*BlockWidth + J*Width*BlockWidth + K;
					end
				end
			end
		end
	end
endfunction

function automatic void InitReceived;
		ref shortreal Received[Height][Length][Width*BlockWidth];
	begin
		integer I, J, K;
		for (I = 0; I < Height; I++) begin
			for (J = 0; J < Length; J++) begin
				for (K = 0; K < Width*BlockWidth; K++) begin
					Received[I][J][K] = NaN;
				end
			end
		end
	end
endfunction

function automatic void Copy;
		ref shortreal From[Height][Length][Width*BlockWidth];
		ref shortreal To[Height][Length][Width*BlockWidth];
	begin
		integer I, J, K;
		for (I = 0; I < Height; I++) begin
			for (J = 0; J < Length; J++) begin
				for (K = 0; K < Width*BlockWidth; K++) begin
					To[I][J][K] = From[I][J][K];
				end
			end
		end
	end
endfunction

function automatic void CalculateResult;
		ref shortreal Data[Height][Length][Width*BlockWidth];
		ref shortreal Result[Height][Length][Width*BlockWidth];
	begin
		shortreal TMP[Height+2][Length+2][Width*BlockWidth+2];
		shortreal Above, Below, East, West, North, South;
		integer I, J, K;
		
		//Top and base levels are null:
		for (J = 0; J < Length+2; J++) begin
			for (K = 0; K < Width*BlockWidth+2; K++) begin
				TMP[0][J][K] = 0;
				TMP[Height+1][J][K] = 0;
			end
		end

		//Front and back walls are null:
		for (I = 0; I < Height+2; I++) begin
			for (K = 0; K < Width*BlockWidth+2; K++) begin
				TMP[I][0][K] = 0;
				TMP[I][Length+1][K] = 0;
			end
		end
		
		//Left and right walls are null:
		for (I = 0; I < Height+2; I++) begin
			for (J = 0; J < Length; J++) begin
				TMP[I][J][0] = 0;
				TMP[I][J][Width*BlockWidth+1] = 0;
			end
		end

		//Remaining cells are copied from Data:
		for (I = 0; I < Height; I++) begin
			for (J = 0; J < Length; J++) begin
				for (K = 0; K < Width*BlockWidth; K++) begin
					TMP[I+1][J+1][K+1] = Data[I][J][K];
				end
			end
		end

		//Calculates values:
		for (I = 1; I <= Height; I++) begin
			for (J = 1; J <= Length; J++) begin
				for (K = 1; K <= Width*BlockWidth; K++) begin
					Above = TMP[I-1][J][K];
					Below = TMP[I+1][J][K];
					East = TMP[I][J][K-1];
					West = TMP[I][J][K+1];
					North = TMP[I][J-1][K];
					South = TMP[I][J+1][K];
					Result[I-1][J-1][K-1] = (Above + Below + East + West + North + South)/6.0;
				end
			end
		end
	end
endfunction
	
task automatic SendData;
		ref shortreal Matrix[Height][Length][Width*BlockWidth];
		ref AXIS_Source_mst_t SourceAgent;
	begin
		axi4stream_transaction Transaction;
		axi4stream_transaction TransactionComplete;
		reg [Precision*BlockWidth-1:0]Data;
		
		Transaction = SourceAgent.driver.create_transaction("Master Transaction");
		Transaction.set_driver_return_item_policy(XIL_AXI4STREAM_NO_RETURN);
		Transaction.set_delay(0);
	
		for (integer I = 0; I < Height; I++) begin
			for (integer J = 0; J < Length; J++) begin
				for (integer K = 0; K < Width; K++) begin
					for (integer L = 0; L < BlockWidth; L++) begin
						Data[L*Precision+:Precision] = $shortrealtobits(Matrix[I][J][K*BlockWidth+L]);
					end

					Transaction.set_data_beat(Data);
					Transaction.set_keep_beat({Precision*BlockWidth/8{1'b1}});
					Transaction.set_last(I == Height-1 && J == Length-1 && K == Width-1);

					SourceAgent.driver.send(Transaction);
				end
			end
		end
	end
endtask

task automatic GetData;
		ref shortreal Matrix[Height][Length][Width*BlockWidth];
		ref AXIS_Sink_slv_t SinkAgent;
	begin
		axi4stream_monitor_transaction Transaction;
		reg [Precision*BlockWidth-1:0]Data;
		reg Last;

		integer I = 0, J = 0, K = 0;
		do begin
			SinkAgent.monitor.item_collected_port.get(Transaction);

			Data = Transaction.get_data_beat();
			Last = Transaction.get_last();
			
			for (integer L = 0; L < BlockWidth; L++) begin
				Matrix[I][J][K*BlockWidth+L] = $bitstoshortreal(Data[L*Precision+:Precision]);
			end
			//$display("Read: [", I, "][", J, "][", K, "]");
			K++;
			if (K >= Width) begin
				J++;
				if (J >= Length) begin
					I++;
					J = 0;
				end
				K = 0;
				if (I > Height) begin
					$error("Unexpected data.");
					$finish;
				end
			end

		end while (!Last);

		if (I < Height) begin
			$error("Missing data.");
			$finish;
		end
	end
endtask

function automatic logic CompareData;
		ref shortreal A[Height][Length][Width*BlockWidth];
		ref shortreal B[Height][Length][Width*BlockWidth];
		shortreal Delta;
	begin
		for (integer I = 0; I < Height; I++) begin
			for (integer J = 0; J < Length; J++) begin
				for (integer K = 0; K < Width*BlockWidth; K++) begin
					Delta = A[I][J][K] - B[I][J][K];
					if (Delta < 0) begin
						Delta = -Delta;
					end
	
					if (Delta > Tolerance) begin
						$error("Data mismatch: %f != %f",  A[I][J][K],  B[I][J][K]);
						return 0;
					end
				end
			end
		end
		return 1;
	end
endfunction;

function automatic void PrintData;
		ref shortreal Matrix[Height][Length][Width*BlockWidth];
	begin
		for (integer I = 0; I < Height; I++) begin
			for (integer J = 0; J < Length; J++) begin
				for (integer K = 0; K < Width; K++) begin
					$write("[");
					for (integer L = 0; L < BlockWidth; L++) begin
						$write("%.2f", $bitstoshortreal(Matrix[I][J][K*BlockWidth+L]));
						if (L+1 < BlockWidth) begin
							$write(" ");
						end
					end
					$write("]");
				end
				$write("\n");
			end
			if (I+1 < Height) begin
				$write("#####\n");
			end
		end
		$write("\n\n");
	end
endfunction;

function automatic void ExportDataCSV;
		ref shortreal Data[Height][Length][Width*BlockWidth];
		input string FileName;
	begin
		integer File;

		File = $fopen(FileName, "w");
		
		if (!File) begin
			$warning({"File could not be opened: ", FileName});
			return;
		end

		for (integer I = 0; I < Height; I++) begin
			for (integer J = 0; J < Length; J++) begin
				for (integer K = 0; K < Width; K++) begin
					$fwrite(File, "[");
					for (integer L = 0; L < BlockWidth; L++) begin
						$fwrite(File, "%.2f", $bitstoshortreal(Data[I][J][K*BlockWidth+L]));
						if (L+1 < BlockWidth) begin
							$fwrite(File, " ");
						end
					end
					$fwrite(File, "]");
				end
				$fwrite(File, "\n");
			end
			if (I+1 < Height) begin
				$fwrite(File, "#####\n");
			end
		end
		$fwrite(File, "\n\n");

		$fclose(File);
	end
endfunction;

task automatic Reset;
		ref M_AXIL_mst_t Master;
	begin
		WriteData(Master, 0, 0);
	end
endtask;

task automatic SetDimensions;
		ref M_AXIL_mst_t Master;
		input integer Length;
		input integer Width;
		input integer Height;
	begin
		WriteData(Master, 4, {Length[0+:AXIL_DataWidth/2], Width[0+:AXIL_DataWidth/2]});
		WriteData(Master, 8, {{AXIL_DataWidth/2{1'h0}}, Height[0+:AXIL_DataWidth/2]});
	end
endtask;

task automatic GetStatus;
		ref M_AXIL_mst_t Master;
		output integer Status;
	begin
		ReadData(Master, 0, Status);
	end
endtask;

task automatic GetMaxDimensions;
		ref M_AXIL_mst_t Master;
		output integer MaxLength;
		output integer MaxWidth;
		output integer MaxHeight;
		integer Data;
	begin
		ReadData(Master, 12, Data);
		MaxLength = Data[AXIL_DataWidth-1:AXIL_DataWidth/2];
		MaxWidth = Data[AXIL_DataWidth/2-1:0];
		ReadData(Master, 16, Data);
		MaxHeight = Data[AXIL_DataWidth/2-1:0];
	end
endtask;

task automatic GetDimensions;
		ref M_AXIL_mst_t Master;
		output integer Length;
		output integer Width;
		output integer Height;
		integer Data;
	begin
		ReadData(Master, 4, Data);
		Length = Data[AXIL_DataWidth-1:AXIL_DataWidth/2];
		Width = Data[AXIL_DataWidth/2-1:0];
		ReadData(Master, 8, Data);
		Height = Data[AXIL_DataWidth/2-1:0];
	end
endtask;

task automatic GetIndexes;
		ref M_AXIL_mst_t Master;
		output integer Line;
		output integer Column;
		output integer Level;
		integer Data;
	begin
		ReadData(Master, 20, Data);
		Line = Data[AXIL_DataWidth-1:AXIL_DataWidth/2];
		Column = Data[AXIL_DataWidth/2-1:0];
		ReadData(Master, 24, Data);
		Level = Data[AXIL_DataWidth/2-1:0];
	end
endtask;

task automatic WriteData;
		ref M_AXIL_mst_t Master;
		input xil_axi_ulong Address;
		input [AXIL_DataWidth-1:0]Data;
	begin
		axi_transaction Command = Master.wr_driver.create_transaction("Write");
		axi_transaction Response;
		Command.set_write_cmd(Address);
		Command.set_driver_return_item_policy(XIL_AXI_PAYLOAD_RETURN);
		Command.set_data_block(Data);
		Master.wr_driver.send(Command);
		Master.wr_driver.wait_rsp(Response);
	end
endtask;

task automatic ReadData;
		ref M_AXIL_mst_t Master;
		input xil_axi_ulong Address;
		output [AXIL_DataWidth-1:0]Data;
	begin
		axi_transaction Command = Master.rd_driver.create_transaction("Read");
		axi_transaction Response;
		Command.set_read_cmd(Address);
		Command.set_driver_return_item_policy(XIL_AXI_PAYLOAD_RETURN);
		Master.rd_driver.send(Command);
		Master.rd_driver.wait_rsp(Response);
		Data = Response.get_data_beat(0);
	end
endtask;

endmodule