`timescale 1s / 1ps
`define Precision 32

module Queue
#(
	parameter MaxLength = 4,
	parameter MaxWidth = 4,
	parameter MaxHeight = 4,
	parameter BlockWidth = 8
)(
	input wire ACLK,
	input wire RSTN,

	input wire [$clog2(MaxLength):0]Length,
	input wire [$clog2(MaxWidth):0]Width,
	input wire [$clog2(MaxHeight):0]Height,

	input wire [BlockWidth*`Precision-1:0]S_TDATA,
	output wire S_TREADY,
	input wire S_TLAST,
	input wire S_TVALID,
	
	output wire [BlockWidth*`Precision-1:0]Above,
	output wire [BlockWidth*`Precision-1:0]North,
	output reg [BlockWidth*`Precision-1:0]East,
	output reg [BlockWidth*`Precision-1:0]Center,
	output wire [BlockWidth*`Precision-1:0]West,
	output wire [BlockWidth*`Precision-1:0]South,
	output wire [BlockWidth*`Precision-1:0]Below,
	
	input wire M_TREADY,
	output wire M_TLAST,
	output wire M_TVALID,

	output reg [$clog2(MaxLength):0]Line,
	output reg [$clog2(MaxWidth):0]Column,
	output reg [$clog2(MaxHeight):0]Level,
	
	output reg Error
);

//Control logic:
wire InputBeat;
wire OutputBeat;
wire LastIn;

assign InputBeat = S_TREADY && S_TVALID;
assign OutputBeat = M_TREADY && M_TVALID;
assign LastIn = (Level == Height-1) && (Line == Length-1) && (Column == Width-1);

`define StateWidth 2

localparam [`StateWidth-1:0]
	S_Reset = `StateWidth'd0,
	S_Pushing = `StateWidth'd1,
	S_Padding = `StateWidth'd2;

reg [`StateWidth-1:0]State;

task IncreaseCounters;
		input wire Enable;
		inout reg [$clog2(MaxHeight):0]Level;
		inout reg [$clog2(MaxLength):0]Line;
		inout reg [$clog2(MaxWidth):0]Column;
	begin
		if (Enable) begin
			if (Column == Width-1) begin
				Column = 0;
				if (Line == Length-1) begin
					Line = 0;
					if (Level == Height) begin
						Level = 0;
					end else begin
						Level = Level + 1;
					end
				end else begin
					Line = Line + 1;
				end
			end else begin
				Column = Column + 1;
			end
		end
	end
endtask

always @(posedge ACLK)
begin
	if (!RSTN) begin
		State <= S_Reset;
		Error <= 0;
	end else if (!Error) begin
		case (State)
			S_Reset: begin
				Level <= 0;
				Line <= 0;
				Column <= 0;
				State <= S_Pushing;
			end
			S_Pushing: begin
				if (InputBeat && (S_TLAST != LastIn)) begin
					Error <= 1;
				end else begin
					if (InputBeat && S_TLAST) begin
						State <= S_Padding;
					end
					IncreaseCounters(InputBeat, Level, Line, Column);
				end
			end
			S_Padding: begin
				if (OutputBeat && Line == Length-1 && Column == Width-1) begin
					State <= S_Reset;
				end
				IncreaseCounters(OutputBeat, Level, Line, Column);
			end
			default: begin
				Error <= 1;
			end
		endcase
	end
end

//AXI signal assignments:
assign S_TREADY = State == S_Pushing && M_TREADY;
assign M_TLAST = State == S_Padding && (Line == Length-1 && Column == Width-1);
assign M_TVALID = (State == S_Pushing && S_TVALID) || State == S_Padding;

//Delay lines' signals:
wire [BlockWidth*`Precision-1:0]DataIn;
wire MoveLine;

assign MoveLine = (State == S_Pushing && InputBeat) || (State == S_Padding && OutputBeat);

assign Below = State == S_Pushing ? S_TDATA : {BlockWidth*`Precision{1'h0}};

wire [$clog2(MaxWidth-2):0]LineDepth;
assign LineDepth = Width - 2;

wire [$clog2(MaxLength*MaxWidth-2):0]LevelDepth;
assign LevelDepth = (Length-1)*Width-1;

FIFO
#(
	.MaxDepth(MaxLength*MaxWidth-2),
	.BlockWidth(BlockWidth)
)PreviousLevel(
	.ACLK(ACLK),
	.RSTN(RSTN),
	
	.Depth(LevelDepth),

	.Enable(MoveLine),
	.DataIn(Below),
	.DataOut(South)
);

FIFO
#(
	.MaxDepth(MaxWidth-2),
	.BlockWidth(BlockWidth)
)PreviousLine(
	.ACLK(ACLK),
	.RSTN(RSTN),
	
	.Depth(LineDepth),

	.Enable(MoveLine),
	.DataIn(South),
	.DataOut(West)
);

always @(posedge ACLK)
begin
	if (MoveLine) begin
		Center <= West;
		East <= Center;
	end
end

FIFO
#(
	.MaxDepth(MaxWidth-2),
	.BlockWidth(BlockWidth)
)NextLine(
	.ACLK(ACLK),
	.RSTN(RSTN),

	.Depth(LineDepth),

	.Enable(MoveLine),
	.DataIn(East),
	.DataOut(North)
);

FIFO
#(
	.MaxDepth(MaxLength*MaxWidth-2),
	.BlockWidth(BlockWidth)
)NextLevel(
	.ACLK(ACLK),
	.RSTN(RSTN),
	
	.Depth(LevelDepth),

	.Enable(MoveLine),
	.DataIn(North),
	.DataOut(Above)
);

endmodule
