`timescale 1s / 1ps
`define  Precision 32

module ALU
(
	input wire ACLK,
	input wire RSTN,
	
	input wire [`Precision-1:0]S_AXIS_A,
	input wire [`Precision-1:0]S_AXIS_B,
	input wire [`Precision-1:0]S_AXIS_C,
	input wire [`Precision-1:0]S_AXIS_D,
	input wire [`Precision-1:0]S_AXIS_E,
	input wire [`Precision-1:0]S_AXIS_F,

	input wire S_AXIS_TVALID,
	input wire S_AXIS_TLAST,
	output wire S_AXIS_TREADY,
	
	output wire [`Precision-1:0]M_AXIS_TDATA,
	output wire M_AXIS_TVALID,
	output wire M_AXIS_TLAST,
	input wire M_AXIS_TREADY
);

wire SumAB_TLAST;
wire SumAB_TVALID;
wire [`Precision-1:0]SumAB_TDATA;

wire SumCD_TVALID;
wire [`Precision-1:0]SumCD_TDATA;

wire SumEF_TVALID;
wire [`Precision-1:0]SumEF_TDATA;

wire SumABCD_TLAST;
wire SumABCD_TVALID;
wire SumABCD_TREADY;
wire [`Precision-1:0]SumABCD_TDATA;

wire SumEFZZ_TVALID;
wire SumEFZZ_TREADY;
wire [`Precision-1:0]Zero = `Precision'h0;
wire [`Precision-1:0]SumEFZZ_TDATA;

wire SumABCDEF_TREADY;

wire Mul_TLAST;
wire Mul_TVALID;
wire Mul_TREADY;
wire [`Precision-1:0]Mul_TDATA;
wire [`Precision-1:0]Sixth = `Precision'h3e2aaaab;

FPSum SumAB
(
	.aclk(ACLK),
	.aresetn(RSTN),

	.s_axis_a_tlast(S_AXIS_TLAST),

	.s_axis_a_tvalid(S_AXIS_TVALID),
	.s_axis_a_tready(S_AXIS_TREADY),
	.s_axis_a_tdata(S_AXIS_A),

	.s_axis_b_tvalid(S_AXIS_TVALID),
	.s_axis_b_tready(),
	.s_axis_b_tdata(S_AXIS_B),

	.m_axis_result_tvalid(SumAB_TVALID),
	.m_axis_result_tready(SumABCD_TREADY),
	.m_axis_result_tdata(SumAB_TDATA),
	.m_axis_result_tlast(SumAB_TLAST)
);

FPSum SumCD
(
	.aclk(ACLK),
	.aresetn(RSTN),

	.s_axis_a_tlast(1'h0),

	.s_axis_a_tvalid(S_AXIS_TVALID),
	.s_axis_a_tready(),
	.s_axis_a_tdata(S_AXIS_C),

	.s_axis_b_tvalid(S_AXIS_TVALID),
	.s_axis_b_tready(),
	.s_axis_b_tdata(S_AXIS_D),

	.m_axis_result_tvalid(SumCD_TVALID),
	.m_axis_result_tready(SumABCD_TREADY),
	.m_axis_result_tdata(SumCD_TDATA),
	.m_axis_result_tlast()
);

FPSum SumEF
(
	.aclk(ACLK),
	.aresetn(RSTN),

	.s_axis_a_tlast(1'h0),

	.s_axis_a_tvalid(S_AXIS_TVALID),
	.s_axis_a_tready(),
	.s_axis_a_tdata(S_AXIS_E),

	.s_axis_b_tvalid(S_AXIS_TVALID),
	.s_axis_b_tready(),
	.s_axis_b_tdata(S_AXIS_F),

	.m_axis_result_tvalid(SumEF_TVALID),
	.m_axis_result_tready(SumEFZZ_TREADY),
	.m_axis_result_tdata(SumEF_TDATA),
	.m_axis_result_tlast()
);

FPSum SumABCD
(
	.aclk(ACLK),
	.aresetn(RSTN),

	.s_axis_a_tlast(SumAB_TLAST),

	.s_axis_a_tvalid(SumAB_TVALID),
	.s_axis_a_tready(SumABCD_TREADY),
	.s_axis_a_tdata(SumAB_TDATA),

	.s_axis_b_tvalid(SumCD_TVALID),
	.s_axis_b_tready(),
	.s_axis_b_tdata(SumCD_TDATA),

	.m_axis_result_tvalid(SumABCD_TVALID),
	.m_axis_result_tready(SumABCDEF_TREADY),
	.m_axis_result_tdata(SumABCD_TDATA),
	.m_axis_result_tlast(SumABCD_TLAST)
);

FPSum SumEFZZ
(
	.aclk(ACLK),
	.aresetn(RSTN),

	.s_axis_a_tlast(1'h0),

	.s_axis_a_tvalid(SumEF_TVALID),
	.s_axis_a_tready(SumEFZZ_TREADY),
	.s_axis_a_tdata(SumEF_TDATA),

	.s_axis_b_tvalid(SumEF_TVALID),
	.s_axis_b_tready(),
	.s_axis_b_tdata(Zero),

	.m_axis_result_tvalid(SumEFZZ_TVALID),
	.m_axis_result_tready(SumABCDEF_TREADY),
	.m_axis_result_tdata(SumEFZZ_TDATA),
	.m_axis_result_tlast()
);

FPSum SumABCDEF
(
	.aclk(ACLK),
	.aresetn(RSTN),

	.s_axis_a_tlast(SumABCD_TLAST),

	.s_axis_a_tvalid(SumABCD_TVALID),
	.s_axis_a_tready(SumABCDEF_TREADY),
	.s_axis_a_tdata(SumABCD_TDATA),

	.s_axis_b_tvalid(SumEFZZ_TVALID),
	.s_axis_b_tready(),
	.s_axis_b_tdata(SumEFZZ_TDATA),

	.m_axis_result_tvalid(Mul_TVALID),
	.m_axis_result_tready(Mul_TREADY),
	.m_axis_result_tdata(Mul_TDATA),
	.m_axis_result_tlast(Mul_TLAST)
);

FPMul Mul
(
	.aclk(ACLK),
	.aresetn(RSTN),

	.s_axis_a_tlast(Mul_TLAST),
	
	.s_axis_a_tvalid(Mul_TVALID),
	.s_axis_a_tready(Mul_TREADY),
	.s_axis_a_tdata(Mul_TDATA),
	
	.s_axis_b_tvalid(Mul_TVALID),
	.s_axis_b_tready(),
	.s_axis_b_tdata(Sixth),

	.m_axis_result_tvalid(M_AXIS_TVALID),
	.m_axis_result_tready(M_AXIS_TREADY),
	.m_axis_result_tdata(M_AXIS_TDATA),
	.m_axis_result_tlast(M_AXIS_TLAST)
);

endmodule