`timescale 1s / 1ps
`define  Precision 32

module FIFO
#(
	parameter MaxDepth = 3,
	parameter BlockWidth = 8
)(
	input wire ACLK,
	input wire RSTN,
	
	input wire [$clog2(MaxDepth):0]Depth,

	input wire Enable,
	input wire [BlockWidth*`Precision-1:0]DataIn,
	output reg [BlockWidth*`Precision-1:0]DataOut
);

reg [$clog2(MaxDepth):0]Index;
reg [BlockWidth*`Precision-1:0]RAM[MaxDepth-1:0];

always @(posedge ACLK)
begin
	if (!RSTN) begin
		Index <= 0;
	end else begin
		if (Enable) begin
			DataOut = RAM[Index];
			RAM[Index] = DataIn;

			if (Index == Depth-1) begin
				Index <= 0;
			end else begin
				Index <= Index + 1;
			end
		end
	end
end

endmodule
