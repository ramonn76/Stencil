`timescale 1s / 1ps
`define  Precision 32

module Laplace
#(
	parameter Max_Length = 4,
	parameter Max_Width = 4,
	parameter Max_Height = 4,
	parameter Block_Width = 8,

	parameter AXIL_Address_Width = 12,
	parameter AXIL_Data_Width = 32
)(
	//General signals:
	input wire ACLK,
	input wire RSTN,
	output wire Error,

	//Slave AXIS interface:
	input wire [Block_Width*`Precision-1:0]S_AXIS_TDATA,
	input wire S_AXIS_TVALID,
	input wire S_AXIS_TLAST,
	//TKEEP is ignored.Always considers a block a unit and valid as a whole.
	input wire [Block_Width*`Precision/8-1:0]S_AXIS_TKEEP,
	output wire S_AXIS_TREADY,
	
	//Master AXIS interface:
	output wire [Block_Width*`Precision-1:0]M_AXIS_TDATA,
	output wire M_AXIS_TVALID,
	output wire M_AXIS_TLAST,
	//TKEEP is set to high. Always considers a block valid as a whole.
	output wire [Block_Width*`Precision/8-1:0] M_AXIS_TKEEP,
	input wire M_AXIS_TREADY,
	
	//AXI Lite interface:
	input wire [AXIL_Address_Width-1:0]S_AXIL_AWADDR,
	input wire S_AXIL_AWVALID,
	output wire S_AXIL_AWREADY,

	input wire [AXIL_Data_Width-1:0]S_AXIL_WDATA,
	input wire S_AXIL_WVALID,
	output wire S_AXIL_WREADY,

	output wire [1:0]S_AXIL_BRESP,
	output wire S_AXIL_BVALID,
	input wire S_AXIL_BREADY,

	input wire [AXIL_Address_Width-1:0]S_AXIL_ARADDR,
	input wire S_AXIL_ARVALID,
	output wire S_AXIL_ARREADY,

	output wire [AXIL_Data_Width-1:0]S_AXIL_RDATA,
	output wire [1:0]S_AXIL_RRESP,
	output wire S_AXIL_RVALID,
	input wire S_AXIL_RREADY
);

//AXIL_Slave signals:
wire MasterRSTN;
wire [$clog2(Max_Length):0]Line;
wire [$clog2(Max_Width):0]Column;
wire [$clog2(Max_Height):0]Level;
wire [$clog2(Max_Length):0]Length;
wire [$clog2(Max_Width):0]Width;
wire [$clog2(Max_Height):0]Height;

//AXIL slave:
AXIL_Slave
#(
	.AddressWidth(AXIL_Address_Width),
	.DataWidth(AXIL_Data_Width),
	.DelayTime(8),
	.MaxLength(Max_Length),
	.MaxWidth(Max_Width),
	.MaxHeight(Max_Height)
)I_AXIL_Slave(
	.S_AXIL_ACLK(ACLK),
	.S_AXIL_ARESETN(RSTN),

	.S_AXIL_AWADDR(S_AXIL_AWADDR),
	.S_AXIL_AWVALID(S_AXIL_AWVALID),
	.S_AXIL_AWREADY(S_AXIL_AWREADY),

	.S_AXIL_WDATA(S_AXIL_WDATA),
	.S_AXIL_WVALID(S_AXIL_WVALID),
	.S_AXIL_WREADY(S_AXIL_WREADY),

	.S_AXIL_BRESP(S_AXIL_BRESP),
	.S_AXIL_BVALID(S_AXIL_BVALID),
	.S_AXIL_BREADY(S_AXIL_BREADY),

	.S_AXIL_ARADDR(S_AXIL_ARADDR),
	.S_AXIL_ARVALID(S_AXIL_ARVALID),
	.S_AXIL_ARREADY(S_AXIL_ARREADY),

	.S_AXIL_RDATA(S_AXIL_RDATA),
	.S_AXIL_RRESP(S_AXIL_RRESP),
	.S_AXIL_RVALID(S_AXIL_RVALID),
	.S_AXIL_RREADY(S_AXIL_RREADY),
	
	.Error(Error),
	.Line(Line),
	.Column(Column),
	.Level(Level),

	.RSTN(MasterRSTN),
	.Length(Length),
	.Width(Width),
	.Height(Height)
);

//Queue signals:
wire [Block_Width*`Precision-1:0]Above;
wire [Block_Width*`Precision-1:0]North;
wire [Block_Width*`Precision-1:0]East;
wire [Block_Width*`Precision-1:0]Center;
wire [Block_Width*`Precision-1:0]West;
wire [Block_Width*`Precision-1:0]South;
wire [Block_Width*`Precision-1:0]Below;

//BlockALU signals:
wire [Block_Width*`Precision-1:0]Above_;
wire [Block_Width*`Precision-1:0]North_;
wire [Block_Width*`Precision-1:0]East_;
wire [Block_Width*`Precision-1:0]West_;
wire [Block_Width*`Precision-1:0]South_;

wire ALU_S_TREADY;
wire ALU_S_TLAST;
wire ALU_S_TVALID;

Queue
#(
	.MaxLength(Max_Length),
	.MaxWidth(Max_Width),
	.MaxHeight(Max_Height),
	.BlockWidth(Block_Width)
)I_Queue(
	.ACLK(ACLK),
	.RSTN(MasterRSTN),

	.Length(Length),
	.Width(Width),
	.Height(Height),

	.S_TDATA(S_AXIS_TDATA),
	.S_TREADY(S_AXIS_TREADY),
	.S_TLAST(S_AXIS_TLAST),
	.S_TVALID(S_AXIS_TVALID),

	.Above(Above),
	.North(North),
	.East(East),
	.Center(Center),
	.West(West),
	.South(South),
	.Below(Below),
	
	.M_TREADY(ALU_S_TREADY),
	.M_TLAST(ALU_S_TLAST),
	.M_TVALID(ALU_S_TVALID),

	.Line(Line),
	.Column(Column),
	.Level(Level),
	
	.Error(Error)
);

Switch
#(
	.MaxLength(Max_Length),
	.MaxWidth(Max_Width),
	.MaxHeight(Max_Height),
	.BlockWidth(Block_Width)
)I_Switch(
	.Length(Length),
	.Width(Width),
	.Height(Height),

	.Line(Line),
	.Column(Column),
	.Level(Level),

	.Above(Above),
	.North(North),
	.East(East),
	.Center(Center),
	.West(West),
	.South(South),

	.Above_(Above_),
	.North_(North_),
	.East_(East_),
	.West_(West_),
	.South_(South_)
);

BlockALU
#(
	.Width(Block_Width)
)I_BlockALU(
	.ACLK(ACLK),
	.RSTN(MasterRSTN && !Error),

	.A(Above_),
	.B(North_),
	.C(East_),
	.D(West_),
	.E(South_),
	.F(Below),

	.S_AXIS_TVALID(ALU_S_TVALID && Level != 0),
	.S_AXIS_TLAST(ALU_S_TLAST),
	.S_AXIS_TREADY(ALU_S_TREADY),
	
	.M_AXIS_TDATA(M_AXIS_TDATA),
	.M_AXIS_TKEEP(M_AXIS_TKEEP),
	.M_AXIS_TVALID(M_AXIS_TVALID),
	.M_AXIS_TLAST(M_AXIS_TLAST),
	.M_AXIS_TREADY(M_AXIS_TREADY)
);

endmodule