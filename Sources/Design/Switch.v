`timescale 1s / 1ps
`define  Precision 32

module Switch
#(
	parameter MaxLength = 4,
	parameter MaxWidth = 4,
	parameter MaxHeight = 4,
	parameter BlockWidth = 8
)(
	input wire [$clog2(MaxLength):0]Length,
	input wire [$clog2(MaxWidth):0]Width,
	input wire [$clog2(MaxHeight):0]Height,

	input wire [$clog2(MaxLength):0]Line,
	input wire [$clog2(MaxWidth):0]Column,
	input wire [$clog2(MaxHeight):0]Level,
	
	input wire [BlockWidth*`Precision-1:0]Above,
	input wire [BlockWidth*`Precision-1:0]North,
	input wire [BlockWidth*`Precision-1:0]East,
	input wire [BlockWidth*`Precision-1:0]Center,
	input wire [BlockWidth*`Precision-1:0]West,
	input wire [BlockWidth*`Precision-1:0]South,
	
	output wire [BlockWidth*`Precision-1:0]Above_,
	output wire [BlockWidth*`Precision-1:0]North_,
	output wire [BlockWidth*`Precision-1:0]East_,
	output wire [BlockWidth*`Precision-1:0]West_,
	output wire [BlockWidth*`Precision-1:0]South_
);

wire [`Precision-1:0] Zero;
assign Zero = `Precision'h0;

wire [`Precision-1:0] AppendLeft;
wire [`Precision-1:0] AppendRight;

generate
	//Sets Above,North and South accordingly, considering they may come from halo:
	assign North_ = Line ? North : {BlockWidth{Zero}};
	assign South_ = Line != Length-1 ? South : {BlockWidth{Zero}};
	assign Above_ = Level > 1 ? Above : {BlockWidth{Zero}};
	//Leftmost from East may come from halo:
	assign AppendLeft = Column ? East[(BlockWidth-1)*`Precision+:`Precision] : Zero;
	//East elements come from neighbours to the left:
	assign East_ = {Center[(BlockWidth-1)*`Precision-1:0], AppendLeft};

	//Rightmost from West may come from halo:
	assign AppendRight = Column != Width-1 ? West[`Precision-1:0] : Zero;
	//West elements come from neighbours to the right:
	assign West_ = {AppendRight, Center[BlockWidth*`Precision-1:`Precision]};
endgenerate

endmodule
