`timescale 1s / 1ps
`define  Precision 32

module BlockALU
#(
	parameter Width = 8
)(
	input wire ACLK,
	input wire RSTN,
	
	input wire [Width*`Precision-1:0]A,
	input wire [Width*`Precision-1:0]B,
	input wire [Width*`Precision-1:0]C,
	input wire [Width*`Precision-1:0]D,
	input wire [Width*`Precision-1:0]E,
	input wire [Width*`Precision-1:0]F,

	input wire S_AXIS_TVALID,
	input wire S_AXIS_TLAST,
	output wire S_AXIS_TREADY,
	
	output wire [Width*`Precision-1:0]M_AXIS_TDATA,
	output wire [Width*`Precision/8-1:0]M_AXIS_TKEEP,
	output wire M_AXIS_TVALID,
	output wire M_AXIS_TLAST,
	input wire M_AXIS_TREADY
);

wire [Width-1:0]M_ALU_Valid;
wire [Width-1:0]M_ALU_Last;
wire [Width-1:0]S_ALU_Ready;

assign M_AXIS_TVALID = M_ALU_Valid[0];
assign M_AXIS_TLAST = M_ALU_Last[0];
assign M_AXIS_TKEEP = {Width*`Precision/8{1'h1}};
assign S_AXIS_TREADY = S_ALU_Ready[0];

generate
	for (genvar I = 0; I < Width; I = I + 1) begin
		ALU I_ALU
		(
			.ACLK(ACLK),
			.RSTN(RSTN),
	
			.S_AXIS_A(A[I*`Precision+:`Precision]),
			.S_AXIS_B(B[I*`Precision+:`Precision]),
			.S_AXIS_C(C[I*`Precision+:`Precision]),
			.S_AXIS_D(D[I*`Precision+:`Precision]),
			.S_AXIS_E(E[I*`Precision+:`Precision]),
			.S_AXIS_F(F[I*`Precision+:`Precision]),

			.S_AXIS_TVALID(S_AXIS_TVALID),
			.S_AXIS_TLAST(S_AXIS_TLAST),
			.S_AXIS_TREADY(S_ALU_Ready[I]),
				
			.M_AXIS_TDATA(M_AXIS_TDATA[I*`Precision+:`Precision]),
			.M_AXIS_TVALID(M_ALU_Valid[I]),
			.M_AXIS_TLAST(M_ALU_Last[I]),
			.M_AXIS_TREADY(M_AXIS_TREADY)
		);
	end
endgenerate

endmodule