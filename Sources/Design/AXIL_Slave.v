`timescale 1 ns / 1 ps

module AXIL_Slave
#(
	parameter AddressWidth = 8,
	parameter DataWidth = 32,
	parameter DelayTime = 8,
	parameter MaxLength = 4,
	parameter MaxWidth = 4,
	parameter MaxHeight = 4
)
(
	input wire S_AXIL_ACLK,
	input wire S_AXIL_ARESETN,

	input wire [AddressWidth-1:0]S_AXIL_AWADDR,
	input wire S_AXIL_AWVALID,
	output wire S_AXIL_AWREADY,

	input wire [DataWidth-1:0]S_AXIL_WDATA,
	input wire S_AXIL_WVALID,
	output wire S_AXIL_WREADY,

	output reg [1:0]S_AXIL_BRESP,
	output reg S_AXIL_BVALID,
	input wire S_AXIL_BREADY,

	input wire [AddressWidth-1:0]S_AXIL_ARADDR,
	input wire S_AXIL_ARVALID,
	output wire S_AXIL_ARREADY,

	output reg [DataWidth-1:0]S_AXIL_RDATA,
	output reg [1:0]S_AXIL_RRESP,
	output reg S_AXIL_RVALID,
	input wire S_AXIL_RREADY,
	
	input wire Error,
	input wire [$clog2(MaxLength):0]Line,
	input wire [$clog2(MaxWidth):0]Column,
	input wire [$clog2(MaxHeight):0]Level,

	output reg RSTN,
	output reg [$clog2(MaxLength):0]Length,
	output reg [$clog2(MaxWidth):0]Width,
	output reg [$clog2(MaxHeight):0]Height
);

reg ResetTrigger;

reg WADDRValid = 0;
reg [AddressWidth-1:0]WADDR	= {AddressWidth{1'h0}};
reg [DataWidth-1:0]WDATA;
reg WDATAValid = 0;

assign S_AXIL_ARREADY = !S_AXIL_RVALID;
assign S_AXIL_AWREADY = !WADDRValid && !S_AXIL_BVALID;
assign S_AXIL_WREADY = !WDATAValid && !S_AXIL_BVALID;

//Address logic:
always @(posedge S_AXIL_ACLK)
begin
	if (!S_AXIL_ARESETN) begin
		WDATAValid <= 0;
		WADDRValid <= 0;
	end else begin
		if (S_AXIL_WVALID && S_AXIL_WREADY) begin
			WDATAValid <= 1;
			WDATA <= S_AXIL_WDATA;
		end
		if (WADDRValid && WDATAValid) begin
			WADDRValid <= 0;
			WDATAValid <= 0;
		end
		if (S_AXIL_AWVALID && S_AXIL_AWREADY) begin
			WADDRValid <= 1;
			WADDR <= S_AXIL_AWADDR;
		end
	end
end

//Read logic:
always @(posedge S_AXIL_ACLK)
begin
	if (!S_AXIL_ARESETN) begin
		S_AXIL_RRESP <= 2'h0;
		S_AXIL_RVALID <= 0;
	end else begin
		if (S_AXIL_RVALID && S_AXIL_RREADY) begin
			S_AXIL_RVALID <= 0;
		end else if (S_AXIL_ARVALID && S_AXIL_ARREADY) begin
			S_AXIL_RRESP <= 2'h0;
			S_AXIL_RVALID <= 1;
			S_AXIL_RDATA <= {DataWidth{1'h0}};
			case (S_AXIL_ARADDR)
				0:
					begin
						S_AXIL_RDATA[0] <= Error;
					end
				4:
					begin
						S_AXIL_RDATA[DataWidth/2+:1+$clog2(MaxLength)] <= Length;
						S_AXIL_RDATA[0+:1+$clog2(MaxWidth)] <= Width;
					end
				8:
					begin
						S_AXIL_RDATA <= Height;
					end
				12:
					begin
						S_AXIL_RDATA[DataWidth/2+:1+$clog2(MaxLength)] <= MaxLength[$clog2(MaxLength):0];
						S_AXIL_RDATA[0+:1+$clog2(MaxWidth)] <= MaxWidth[$clog2(MaxWidth):0];
					end
				16:
					begin
						S_AXIL_RDATA <= MaxHeight;
					end
				20:
					begin
						S_AXIL_RDATA[DataWidth/2+:1+$clog2(MaxLength)] <= Line;
						S_AXIL_RDATA[0+:1+$clog2(MaxWidth)] <= Column;
					end
				24:
					begin
						S_AXIL_RDATA <= Level;
					end
				default:
					S_AXIL_RRESP <= 2'h2;
			endcase
		end
	end
end

//Write logic:
always @(posedge S_AXIL_ACLK)
begin
	if (!S_AXIL_ARESETN) begin
		S_AXIL_BVALID <= 0;
		S_AXIL_BRESP <= 2'h0;

		ResetTrigger <= 0;
		Length <= MaxLength;
		Width <= MaxWidth;
		Height <= MaxHeight;
	end else begin
		ResetTrigger <= 0;
		if (S_AXIL_BVALID && S_AXIL_BREADY) begin
			S_AXIL_BVALID <= 0;
		end else if (WADDRValid && WDATAValid) begin
			S_AXIL_BVALID <= 1;
			S_AXIL_BRESP <= 2'h0;
			case (WADDR)
				0:
					begin
						ResetTrigger <= 1;
					end
				4:
					begin
						if (WDATA[DataWidth/2+:1+$clog2(MaxLength)] <= MaxLength) begin
							//Length <= WDATA[DataWidth/2+:1+$clog2(MaxLength)];
							ResetTrigger <= 1;
						end
						if (WDATA[0+:1+$clog2(MaxWidth)] <= MaxWidth) begin
							//Width <= WDATA[0+:1+$clog2(MaxWidth)];
							ResetTrigger <= 1;
						end
					end
				8:
					begin
						if (WDATA[0+:1+$clog2(MaxHeight)] <= MaxHeight) begin
							//Height <= WDATA[0+:1+$clog2(MaxHeight)];
							ResetTrigger <= 1;
						end
					end
				default:
					S_AXIL_BRESP <= 2'h2;
			endcase
		end
	end
end

//Reset logic:
integer Counter;
always @(posedge S_AXIL_ACLK)
begin
	if (!S_AXIL_ARESETN) begin
		RSTN <= 0;
		Counter <= 0;
	end else begin
		if (!Counter) begin
			RSTN <= 1;
		end
		if (ResetTrigger) begin
			Counter <= DelayTime;
			RSTN <= 0;
		end else if (Counter) begin
			Counter <= Counter-1;
		end
	end
end
endmodule
