`timescale 1 ps / 1 ps

module Top
(
	input wire PCIe_PERSTN,
	input wire PCIe_REFCLK_clk_n,
	input wire PCIe_REFCLK_clk_p,
	input wire [7:0]PCIe_X8_rxn,
	input wire [7:0]PCIe_X8_rxp,
	output wire [7:0]PCIe_X8_txn,
	output wire [7:0]PCIe_X8_txp
);

BD I_BD
(
	.PCIe_PERSTN(PCIe_PERSTN),
	.PCIe_REFCLK_clk_n(PCIe_REFCLK_clk_n),
	.PCIe_REFCLK_clk_p(PCIe_REFCLK_clk_p),
	.PCIe_X8_rxn(PCIe_X8_rxn),
	.PCIe_X8_rxp(PCIe_X8_rxp),
	.PCIe_X8_txn(PCIe_X8_txn),
	.PCIe_X8_txp(PCIe_X8_txp)
);

endmodule